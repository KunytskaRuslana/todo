create database ToDo_KRV;
go
use ToDo_KRV;
go
create table tblTask
(
	Id int not null identity(1,1),
	Title nvarchar(50) not null,
	IsDone int not null,
	constraint PK_TypeRooms_Id primary key (Id)
)
go
set identity_insert tblTask on
insert into tblTask 
		(Id, Title, IsDone) 
	values 
		(1, 'Task 1', 1),
		(2, 'Task 2', 1), 
		(3, 'Task 3', 0),
		(4, 'Task 4', 0),
		(5, 'Task 5', 0),
		(6, 'Task 6', 1)
set identity_insert tblTask off
go
