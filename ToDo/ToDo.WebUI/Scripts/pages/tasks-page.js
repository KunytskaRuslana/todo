﻿(function () {
    console.log("[tasks-page] - test")

    var onIsDoneClick = function (e) {
        console.log("[tasks-page] - checkbox click");
        console.log("[tasks-page] - click", arguments);
        var ch = e.target;
        var isDone = ch.checked;
        console.log("[tasks-page] - click:isDone = " + isDone);
        var taskId = ch.getAttribute("data-task-id");
        console.log("[tasks-page] - click:isDone = " + taskId);
        //AJAX function
        $.ajax({
            url: "/ToDo/ChangeStatus",
            type: "POST",
            dataType: "text",
            data: {
                taskId: taskId,
                isDone: isDone
            }
        }).done(function () {
            console.log("[tasks-page] - done");
            //put a mark on implementation for one task
            if (isDone) {
                $("tr").eq(taskId).attr("class", "trYes");
            }
            else {
                $("tr").eq(taskId).attr("class", "trNo");
            }
        }).fail(function () {
            console.log("[tasks-page] - fail");
        });
    };

    var onIsSelect = function () {
        var select = $("#myselect option:selected").val();
        switch (select) {
            case 'all':
                $('tr').has('input[type="checkbox"]').show();
                break;
            case 'commit':
                var array = $('tr').not($('tr').has('input[type="checkbox"]:checked'));
                array.hide();
                break;
            case 'inprogress':
                $('tr').has('input[type="checkbox"]:checked').hide();
                break;
            default:
                break;
        }
    };
    var sel = function () {
        console.log("[tasks-page] - sel");
        $("select").on("click", onIsSelect);
        console.log("[tasks-page] - click", arguments);
    };

    var init = function () {
        console.log("[tasks-page] - init");
        //select all checkbox in page
        $("input[type='checkbox']").on("click", onIsDoneClick);
        sel();
        //put a mark on implementation for all tasks
        $('tr').has('input[type="checkbox"]:checked').attr("class", "trYes");
    };

    $(function () {
        console.log("[tasks-page] - main");
        init();
    });
    
})();