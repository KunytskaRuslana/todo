﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDo.Entities;
using ToDo.Repositories;
using ToDo.WebUI.Models;

namespace ToDo.WebUI.Controllers
{
    public class ToDoController : Controller
    {
        private readonly ITaskRepository _taskRepository;

        public ToDoController()
        {
            //this._taskRepository = new FakeTaskRepository();
            this._taskRepository = new SqlTaskRepository();
        }

        //
        // GET: /ToDo/
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Tasks = _taskRepository.SelectAll();
            return View();
        }

        // POST: /ToDo/
        [HttpPost]
        public ActionResult Index(string txtTask)
        {
            if (string.IsNullOrWhiteSpace(txtTask))
            {
                ViewBag.Error = "Error! The task can not be empty!";
            }
            else if (txtTask.Length > 10)
            {
                ViewBag.TaskTitle = txtTask;
                ViewBag.Error = "Error! The task can not be more 10 symbols!";
            }
            else
            {
                TaskEntity taskEntity = new TaskEntity();
                taskEntity.Title = txtTask;
                taskEntity.IsDone = true;
                _taskRepository.AddTask(taskEntity);
            }
            ViewBag.Tasks = _taskRepository.SelectAll();
            return View();
        }

        // POST: /ToDo/
        [HttpPost]
        public ActionResult Delete(int taskId)
        {
            _taskRepository.DeleteTask(taskId);
            ViewBag.Tasks = _taskRepository.SelectAll();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            TaskEntity task = this._taskRepository.GetId(id);
            TaskModel taskModel = new TaskModel()
            {
                Id = task.Id,
                Title = task.Title,
                IsDone = task.IsDone
            };
            return View(taskModel);
        }

        [HttpPost]
        public ActionResult Edit(TaskModel taskModel)
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ChangeStatus(int taskId, bool isDone)
        {
            TaskEntity task = new TaskEntity();
            task.Id = taskId;
            task.IsDone = isDone;
            _taskRepository.UpdateStatusTask(task);
            ViewBag.Tasks = _taskRepository.SelectAll();
            return RedirectToAction("Index");
        }

    }
}
