﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDo.Entities;

namespace ToDo.Repositories
{
    public class SqlTaskRepository : ITaskRepository
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;
        static string querySelect = "SELECT [Id],[Title],[IsDone]"
                                    + "FROM [dbo].[tblTask]";
        static string queryInsert = "INSERT INTO [dbo].[tblTask] ([Title],[IsDone]) VALUES (@Title,@IsDone); SELECT CAST(SCOPE_IDENTITY() AS INT);";
        static string queryDelete = "DELETE FROM [dbo].[tblTask] WHERE [Id] = @Id";
        static string queryGetAllId = "SELECT [Id],[Title],[IsDone]"
                             + "FROM [dbo].[tblTask]"
                             + "WHERE [Id] = @Id";
        static string queryUpdate = "UPDATE [dbo].[tblTask] SET [IsDone] = @IsDone WHERE [Id] = @id";

        public List<TaskEntity> SelectAll()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(querySelect, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<TaskEntity> taskEntity = new List<TaskEntity>();
                        while (reader.Read())
                        {
                            taskEntity.Add(new TaskEntity()
                            {
                                Id = (int)reader["id"],
                                Title = (string)reader["Title"],
                                IsDone = (((int)reader["IsDone"]) == 1)?true:false
                            });
                        }
                        return taskEntity;
                    }
                }
            }
        }

        public TaskEntity AddTask(TaskEntity taskEntity)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(queryInsert, connection))
                {
                    command.Parameters.AddWithValue("Title", taskEntity.Title);
                    command.Parameters.AddWithValue("IsDone", taskEntity.IsDone);

                    int connectId = (int)command.ExecuteScalar();
                    taskEntity.Id = connectId;

                    return taskEntity;
                }
            }
        }

        public void DeleteTask(int taskId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(queryDelete, connection))
                {
                    command.Parameters.AddWithValue("Id", taskId);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateStatusTask(TaskEntity taskEntity)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(queryUpdate, connection))
                {
                    command.Parameters.AddWithValue("Id", taskEntity.Id);
                    command.Parameters.AddWithValue("IsDone", taskEntity.IsDone);

                    command.ExecuteNonQuery();

                }
            }
        }

        public TaskEntity GetId(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
                {
                    command.Parameters.AddWithValue("id", id);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        TaskEntity taskEntity = new TaskEntity();
                        if (reader.Read())
                        {
                            taskEntity = (new TaskEntity()
                            {
                                Id = (int)reader["Id"],
                                Title = (string)reader["Title"],
                                IsDone = (((int)reader["IsDone"]) == 1) ? true : false
                            });
                            return taskEntity;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
    }
}
