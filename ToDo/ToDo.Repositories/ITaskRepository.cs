﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDo.Entities;

namespace ToDo.Repositories
{
    public interface ITaskRepository
    {
        List<TaskEntity> SelectAll();
        TaskEntity AddTask(TaskEntity taskEntity);
        void DeleteTask(int taskId);
        void UpdateStatusTask(TaskEntity taskEntity);
        TaskEntity GetId(int id);
    }
}
